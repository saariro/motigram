import React  from 'react';
import {Route} from 'react-router';
import App from './components/App';
import Login from './components/login/LoginPage';
import VerifyLoggedIn from './components/VerifyLoggedIn';


const NotFound = () => (
  <h1>404.. This page is not found!</h1>);

export default (
	<Route component={VerifyLoggedIn}>
		<Route path="/login" component={Login}/>
		<Route path="/" component={App}/>
		<Route path='*' component={NotFound} />
	</Route>
);
