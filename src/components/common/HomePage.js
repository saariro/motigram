import React from 'react';
import { connect } from 'react-redux';
import {Card, CardActions, CardHeader, CardMedia, CardText} from 'material-ui/Card';
import FlatButton from 'material-ui/FlatButton';
import * as postActions from '../../actions/postActions';
import * as loginActions from '../../actions/loginActions';
import RaisedButton from 'material-ui/RaisedButton';


const styles = {
  image: {
    maxWidth: '100%',
    maxHeight: '100%',
    paddingLeft: '10px',
    paddingRight: '10px',
    display : 'block',
    margin : 'auto'
    },
  container: {
    maxWidth: '60%',
    maxHeight: '60%',
    textAlign: 'center',
    margin: 'auto',
    display: 'block',
    },
  card: {
    marginTop: '20px',
    maxHeight: '60%',
    },
};

class Home extends React.Component{
  constructor(props){
    super(props);
    this.increasePostIndex = this.increasePostIndex.bind(this);
    this.decreasePostIndex = this.decreasePostIndex.bind(this);
    this.like = this.like.bind(this);
    this.toggleProfile = this.toggleProfile.bind(this);
  }
    componentDidMount() {
      this.props.getPosts().then(() => {
        if (this.props.posts !== null && this.props.posts[this.props.index] !== null) {
            this.props.getFile(this.props.posts[this.props.index].photo);
        }
    });
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.index !== this.props.index) {
            this.props.getFile(nextProps.posts[nextProps.index].photo);
        }

    }

    increasePostIndex(){
      this.props.increasePostIndex();
    }

    decreasePostIndex(){
      this.props.decreasePostIndex();
    }

    toggleProfile(){
      this.props.toggleProfile();
    }

    like(){
        let updatedPost = this.props.posts[this.props.index];
        updatedPost.likes++;
        this.props.updatePost(updatedPost, updatedPost._id);
    }
  render(){

      const ImageCard = () => (
          <Card style = {styles.card}>
          <CardHeader className='user-name' title={this.props.credentials[this.props.credentials.length-1].userName} subtitle={'Amount of likes: ' + this.props.posts[this.props.index].likes}  onClick={this.toggleProfile}/>
            <CardMedia style = {styles.container}>
              <img style={styles.image} src={this.props.files.file}/>
            </CardMedia>
            <CardText>
            {this.props.posts[this.props.index].caption}
            </CardText>
            <CardActions>
              <FlatButton disabled={this.props.index === 0} label="Previous picture" onTouchTap={this.decreasePostIndex} />
              <FlatButton disabled={this.props.index === this.props.posts.length-1} label="Next picture" onTouchTap={this.increasePostIndex}/>
            </CardActions>
            <LikeButton/>
          </Card>
    );

    const PostsButton = () => (
      <div>
        <FlatButton label="Create Post" primary={true} onTouchTap={this.props.toggleProfile}/>
      </div>
    );


    const LikeButton = () => (
        <RaisedButton primary={true} label="Like" onTouchTap={e => {
                e.preventDefault();
                this.like();
              }}/>
    );

    if (this.props.posts !== 'undefined' && this.props.posts.length > 0) {
      return (
      <div className='container'>
        <ImageCard/>

      </div>
      );
    }
    else {
      return (
        <div className='container'>
          <h1>Motigram</h1>

          <p>Welcome to Motigram! You currently do not have any posts, click the button below to add your first picture</p>
          <PostsButton/>
        </div>
      );
    }
}
}

const mapStateToProps = (state) => {
  return {
    posts: state.posts,
    files: state.files,
    index: state.index,
    credentials: state.credentials
  };
};

// Maps actions to props
const mapDispatchToProps = (dispatch) => {
  return {
    getPosts: () => dispatch(postActions.getPosts()),
    getFile: (imageId) => dispatch(postActions.getFile(imageId)),
    increasePostIndex: () => dispatch(postActions.increaseIndex()),
    decreasePostIndex: () => dispatch(postActions.decreaseIndex()),
    updatePost: (newPost, id) => dispatch(postActions.updatePost(newPost, id)),
    toggleProfile: () => dispatch(loginActions.toggleProfile())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);