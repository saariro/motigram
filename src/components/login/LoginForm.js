import React from 'react';
import { connect } from 'react-redux';
import * as loginActions from '../../actions/loginActions';

import FlatButton from 'material-ui/FlatButton';
import TextField from 'material-ui/TextField';
import Paper from 'material-ui/Paper';

const styles = {
  paper: {
      height: '100%',
      width: '100%',
      marginTop: '25%',
      textAlign: 'center',
      verticalAlign: 'middle',
      display: 'inline-block',
      minHeight: '300px',
      minWidth: '300px',
    },
  loginFields: {
    marginTop: '1em',
    },
  loginButton: {
    marginTop: '1em',
    },
  loginContainer: {
    width: '50%',
    minWidth: '400px',
    margin: 'auto',
    display: 'block',
    },
	warningText: {
      color: '#FF0000',
    },
};

class LoginForm extends React.Component{
  constructor(props){
    super(props);
  }

  login(input){
    this.props.login(input);
  }

  render(){
    let userInput = '';
    let passwordInput = '';
    const LoginField= () => (
      <div>
        <TextField style={styles.loginFields}
          hintText='Username' onChange={e => {
                e.preventDefault();
                userInput = e.target.value;
              }}
        /><br/>
        <TextField style={styles.loginFields}
          hintText='Password' type='password' onChange={e => {
                e.preventDefault();
                passwordInput = e.target.value;
              }}
        /><br/>
      </div>
    );

    const LoginButton = () => (
      <div>
        <FlatButton style={styles.loginButton} label='Login' primary={true} fullWidth={true} onTouchTap={e => {
                e.preventDefault();
                let input = {username: userInput, password:passwordInput};
                this.login(input);
              }}/>
      </div>
    );
    const LoginForm = () => (
      <div style={styles.loginContainer} className='login-form'>
        <Paper style={styles.paper} zDepth={3}>
        <h2 className='form-signin-heading'>Welcome to Motigram</h2>
        <LoginField/>
        {this.props.credentials[this.props.credentials.length -1].statusText !== null ? <p style={styles.warningText}>{this.props.credentials[this.props.credentials.length -1].statusText}</p> : null }
        <LoginButton/>
        </Paper>
      </div>
    );
    return(
        <LoginForm/>
    );
  }
}

// Maps state from store to props
const mapStateToProps = (state) => {
  return {
    credentials: state.credentials
  };
};

// Maps actions to props
const mapDispatchToProps = (dispatch) => {
  return {
    login: credential => dispatch(loginActions.login(credential))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(LoginForm);