import React from 'react';
import { connect } from 'react-redux';
import LoginForm from './LoginForm';

import {cyan200} from 'material-ui/styles/colors';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';



const muiTheme = getMuiTheme({
  palette: {
    accent1Color: cyan200,
  },
});


class Login extends React.Component{
  constructor(props){
    super(props);
  }

  render(){
    return(
      <MuiThemeProvider muiTheme={muiTheme}>
        <div className='container'>
          <LoginForm/>
        </div>
      </MuiThemeProvider>
    );
  }
}

// Maps state from store to props
const mapStateToProps = (state) => {
  return {
    credentials: state.credentials
  };
};

export default connect(mapStateToProps, null)(Login);