import React  from 'react';
import { connect } from 'react-redux';
import {browserHistory } from 'react-router';

class VerifyLoggedIn extends React.Component {
  componentDidMount() {
    if (!this.props.loginInfo[this.props.loginInfo.length -1].isLoggedIn) {
      browserHistory.replace("/login");
    }
  }

  render() {
      return this.props.children;
  }
}

// Grab a reference to the current URL. If this is a web app and you are
// using React Router, you can use `ownProps` to find the URL. Other
// platforms (Native) or routing libraries have similar ways to find
// the current position in the app.
function mapStateToProps(state, ownProps) {
  return {
    loginInfo: state.credentials,
    currentURL: ownProps.location.pathname
  };
}

export default connect(mapStateToProps)(VerifyLoggedIn);