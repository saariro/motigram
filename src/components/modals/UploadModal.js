// ./src/components/common/HomePage.js
import React from 'react';
import { connect } from 'react-redux';
import * as postActions from '../../actions/postActions';
import * as loginActions from '../../actions/loginActions';
import FlatButton from 'material-ui/FlatButton';
import Dialog from 'material-ui/Dialog';
import Dropzone from 'react-dropzone';
import TextField from 'material-ui/TextField';

const styles = {
  image: {
    height: '100%',
  },
  imageParent: {
    textAlign: 'center',
    height: '100%',
  },
  imageText: {
    marginTop: '40%',
  },
};

class ImageUpload extends React.Component{
constructor(props){
    super(props);
  }
  
  uploadFiles(caption){   
	this.props.uploadFiles(this.props.file).then(() => {
		let newPost = {photo: this.props.files.uploadedFile, caption: caption, likes: 0};
		this.props.createPost(newPost).then(() => {
			this.props.getPosts()
			});
    });
  }
   
	componentWillReceiveProps(nextProps) {
		if (nextProps.files.uploadedFile !== null && nextProps.files.uploadedFile !== this.props.files.uploadedFile) {
			this.props.toggleModal();
		}
		
	}
  onDrop(files) {
    this.props.prepareUpload(files);
  }
  
  render(){
    let captionInput;
    const standardActions = [
      <FlatButton
        label='Cancel'
        primary={true}
        onTouchTap={this.props.toggleModal}/>,
      <FlatButton
        label='Upload'
        primary={true}
		disabled={this.props.file === null}
        onTouchTap={e => {
                e.preventDefault();
                this.uploadFiles(this.refs.caption.input.value);
              }}/>,
    ];
      return (
          <Dialog
            open={this.props.isOpen}
			modal={true}
            title='Create a new post'
            actions={standardActions}
            onRequestClose={this.props.toggleModal}>
             <section>
                <div className='dropzone'>
                  <Dropzone onDrop={this.onDrop.bind(this)} accept='image/jpeg, image/png'>
                    {this.props.file === null ? <p style={styles.imageText}>Drop image here to upload</p> : <div style={styles.imageParent}>{<img style={styles.image} src={this.props.file.preview} />}</div> }
                  </Dropzone>
                </div>
                 <TextField hintText='Caption' ref='caption'/>
              </section>
          </Dialog>
      );
    }
}

const mapStateToProps = (state) => {
  return {
    isOpen: state.isOpen.modalOpen,
    credentials: state.credentials,
    posts: state.posts,
    file: state.file,
    files: state.files
  };
};

// Maps actions to props
const mapDispatchToProps = (dispatch) => {
  return {
    toggleModal: () => dispatch(loginActions.toggleModal()),
    prepareUpload: (post) => dispatch(postActions.prepareUpload(post)),
    uploadFiles: (file) => dispatch(postActions.uploadFiles(file)),
    getPosts: () => dispatch(postActions.getPosts()),
    createPost: (newPost) => dispatch(postActions.createPost(newPost))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ImageUpload);