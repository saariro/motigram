// ./src/components/common/HomePage.js
import React from 'react';
import { connect } from 'react-redux';
import * as loginActions from '../../actions/loginActions';
import FlatButton from 'material-ui/FlatButton';
import Dialog from 'material-ui/Dialog';


class Profile extends React.Component{
  constructor(props){
    super(props);
  }

  render(){
    const standardActions = [
      <FlatButton
        label="Close"
        primary={true}
        onTouchTap={this.props.toggleProfile}/>
  ];
      return (
          <Dialog
            open={this.props.isOpen}
            title='Your profile'
            actions={standardActions}
            onRequestClose={this.props.toggleProfile}>
             <section>
                <div>
                    <h3>Username: {this.props.credentials[this.props.credentials.length -1].userName}</h3>
                    <h3>First name: {this.props.credentials[this.props.credentials.length -1].firstName}</h3>
                    <h3>Last name: {this.props.credentials[this.props.credentials.length -1].lastName}</h3>
                    <h3>Email: {this.props.credentials[this.props.credentials.length -1].email}</h3>
                </div>
              </section>
          </Dialog>
      );
    }
}

const mapStateToProps = (state) => {
  return {
    isOpen: state.isOpen.profileOpen,
    credentials: state.credentials
  };
};

// Maps actions to props
const mapDispatchToProps = (dispatch) => {
  return {
    toggleProfile: () => dispatch(loginActions.toggleProfile()) 
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Profile);