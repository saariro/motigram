// ./src/components/App.js
import React  from 'react';
import { connect } from 'react-redux';
import {cyan200} from 'material-ui/styles/colors';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import IconMenu from 'material-ui/IconMenu';
import IconButton from 'material-ui/IconButton';
import NavigationExpandMoreIcon from 'material-ui/svg-icons/navigation/expand-more';
import MenuItem from 'material-ui/MenuItem';
import {Toolbar, ToolbarGroup, ToolbarSeparator, ToolbarTitle} from 'material-ui/Toolbar';
import * as loginActions from '../actions/loginActions';
import Profile from './modals/ProfileModal';
import ImageUpload from './modals/UploadModal';
import Home from './common/HomePage';

const styles = {
  container: {
    textAlign: 'center',
  },
};


const muiTheme = getMuiTheme({
  palette: {
    accent1Color: cyan200,
  },
});


class App extends React.Component{
  constructor(props) {
    super(props);
    this.toggleProfile = this.toggleProfile.bind(this);
  }

    toggleProfile(){
      this.props.toggleProfile();
    }
  render(){

    const MotigramToolBar = () => (
      <Toolbar>
            <ToolbarGroup>
              <ToolbarTitle className='user-name' text= {this.props.credentials[this.props.credentials.length -1].userName}  onClick={this.toggleProfile}/>
              <ToolbarSeparator />
              <IconMenu iconButtonElement={
                    <IconButton touch={true}>
                    <NavigationExpandMoreIcon />
                    </IconButton>
                    }>
                <MenuItem primaryText="Create new post" onTouchTap={this.props.toggleModal} />
                <MenuItem primaryText="Logout" onTouchTap={this.props.logout}/>
              </IconMenu>
            </ToolbarGroup>
      </Toolbar>
    );
      return (
          <MuiThemeProvider muiTheme={muiTheme}>
            <div style={styles.container}>
                <MotigramToolBar/>
                <Profile/>
                <ImageUpload/>
                <Home/>
            </div>
          </MuiThemeProvider>
      );
    }
}



const mapStateToProps = (state) => {
  return {
    credentials: state.credentials
  };
};

// Maps actions to props
const mapDispatchToProps = (dispatch) => {
  return {
    toggleModal: () => dispatch(loginActions.toggleModal()),
    logout: () => dispatch(loginActions.logout()),
    toggleProfile: () => dispatch(loginActions.toggleProfile())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(App);