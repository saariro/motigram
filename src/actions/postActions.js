

import Axios from 'axios';
import * as types from '../constants/actionTypes';

const sessionToken = sessionStorage.getItem('jwt-token');

export const postSuccess = (posts) => {
  return {
    type: types.GET_POST_SUCCESS,
    posts
  };
};
export const postFailure = (response) => {
  return {
    type: types.GET_POST_FAILURE,
    response
  };
};

export const getPosts = () => {
  return (dispatch) => {
    return Axios.get(types.POSTURL, {headers: {'Authorization': sessionStorage.getItem('jwt-token')}})
      .then(response => {
        dispatch(postSuccess(response.data));
      })
      .catch(error => {
        dispatch(postFailure(error.data));
      });
  };
};


export const postFileSuccess = (posts) => {
  return {
    type: types.POST_FILE_SUCCESS,
    posts
  };
};
export const postFileFailure = (response) => {
  return {
    type: types.POST_FILE_FAILURE,
    response
  };
};

export const uploadFiles = (file) => {
  let data = new FormData();
  data.append('data', file);
  return (dispatch) => {
    return Axios.post(types.FILEURL, data, {headers: {'Authorization': sessionToken, 'Content-Type': 'multipart/form-data'}})
      .then(response => {
        dispatch(postFileSuccess(response.data));
      })
      .catch(error => {
        dispatch(postFileFailure(error.data));
      });
  };
};

export const createPostSuccess = (posts) => {
  return {
    type: types.CREATE_POST_SUCCESS,
    posts
  };
};
export const createPostFailure = (response) => {
  return {
    type: types.CREATE_POST_FAILURE,
    response
  };
};
export const createPost = (newPost) => {
  return (dispatch) => {
    return Axios.post(types.POSTURL, newPost, {headers: {'Authorization': sessionStorage.getItem('jwt-token')}})
      .then(response => {
        dispatch(createPostSuccess(response.data));
      })
      .catch(error => {
        dispatch(createPostFailure(error.data));
      });
  };
};


export const updatePostSuccess = (posts) => {
  return {
    type: types.UPDATE_POST_SUCCESS,
    posts
  };
};
export const updatePostFailure = (response) => {
  return {
    type: types.UPDATE_POST_FAILURE,
    response
  };
};
export const updatePost = (newPost, id) => {
  return (dispatch) => {
    return Axios.post(types.POSTURL.concat(id), newPost, {headers: {'Authorization': sessionStorage.getItem('jwt-token')}})
      .then(response => {
        dispatch(updatePostSuccess(response.data));
      })
      .catch(error => {
        dispatch(updatePostFailure(error.data));
      });
  };
};

export const getFileSuccess = (file) => {
  return {
    type: types.GET_FILE_SUCCESS,
    file
  };
};

export const getFileFailure = (response) => {
  return {
    type: types.GET_FILE_FAILURE,
    response
  };
};
export const getFile = (imageId) => {
  return (dispatch) => {
    return Axios({
      method:'get',
      url:types.FILEURL.concat(imageId),
      responseType:'blob',
      headers: {'Authorization': sessionStorage.getItem('jwt-token')}
    })
      .then(response => {
        let blob = response.data;
        let url = URL.createObjectURL(blob);
        dispatch(getFileSuccess(url));
      })
      .catch(error => {
        dispatch(getFileFailure(error.data));
      });
  };
};

export const prepareUpload = (image) => {
  return {
    type: types.PREPARE_UPLOAD,
    image: image
  };
};


export const increaseIndex = () => {
  return {
    type: types.INCREASE_INDEX
  };
};

export const decreaseIndex = () => {
  return {
    type: types.DECREASE_INDEX
  };
};




export const getCommentSuccess = (comments) => {
  return {
    type: types.GET_COMMENT_SUCCESS,
    comments
  };
};

export const getCommentFailure = (response) => {
  return {
    type: types.GET_COMMENT_FAILURE,
    response
  };
};
export const getComments = (postId) => {
  return (dispatch) => {
    return Axios({
      method:'get',
      url:types.FILEURL.concat(postId + '/comments/'),
      headers: {'Authorization': sessionStorage.getItem('jwt-token')}
    })
      .then(response => {
        dispatch(getCommentSuccess(response.data));
      })
      .catch(error => {
        dispatch(getCommentFailure(error.data));
      });
  };
};

export const createCommentSuccess = (comments) => {
  return {
    type: types.CREATE_COMMENT_SUCCESS,
    comments
  };
};

export const createCommentFailure = (response) => {
  return {
    type: types.CREATE_COMMENT_FAILURE,
    response
  };
};
export const postComment = (text, postId) => {
  return (dispatch) => {
    return Axios.post(types.POSTURL.concat(postId + '/comments'), text, {headers: {'Authorization': sessionStorage.getItem('jwt-token')}})
      .then(response => {
        dispatch(createCommentSuccess(response.data));
      })
      .catch(error => {
        dispatch(createCommentFailure(error.data));
      });
  };
};