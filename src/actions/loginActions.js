
import * as types from '../constants/actionTypes';
import Axios from 'axios';



export const loginSuccess = (credentials) => {
  return {
    type: types.LOGIN_SUCCESS,
    credentials
  };
};
export const loginFailure = (credentials) => {
  return {
    type: types.LOGIN_FAILURE,
    credentials
  };
};

export const login = (credentials) => {
  return (dispatch) => {
    return Axios.post(types.LOGINURL, credentials)
      .then(response => {
		sessionStorage.setItem('jwt-token', response.data.token);
        dispatch(loginSuccess(response.data));
      })
      .catch(error => {
        dispatch(loginFailure(error.data));
      });
  };
};

export const toggleModal = () => {
  return {
    type: types.MODAL_SUCCESS
  };
};

export const toggleProfile = () => {
  return {
    type: types.PROFILE_OPEN
  };
};


export const logout = () => {
	sessionStorage.removeItem('jwt-token');
  return {
    type: types.LOGOUT
  };
};