
import * as types from '../constants/actionTypes';


export default (state =  0, action) => {
  switch (action.type){
    case types.INCREASE_INDEX:
		state++;
        return state;
    case types.DECREASE_INDEX:
		state--;
        return state;
    case types.LOGOUT:
        return 0;
    default:
          return state;
  }
};