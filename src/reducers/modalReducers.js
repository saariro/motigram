
import * as types from '../constants/actionTypes';


export default (state = {modalOpen: false, profileOpen: false}, action) => {
  switch (action.type){
    case types.MODAL_SUCCESS:
        return {modalOpen: !state.modalOpen, profileOpen: state.profileOpen};
    case types.PROFILE_OPEN:
        return {modalOpen: state.modalOpen, profileOpen: !state.profileOpen};
    case types.LOGOUT:
        return {modalOpen: false, profileOpen: false};
    default:
          return state;
  }
};