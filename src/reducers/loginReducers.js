
import {browserHistory} from 'react-router';
import * as types from '../constants/actionTypes';

const initialState = {
    token: null,
    userName: null,
    isLoggedIn: false,
    statusText: null,
	email: null,
	firstName: null,
	lastName: null
};



export default (state = [initialState], action) => {
  switch (action.type){
    case types.LOGIN_SUCCESS:
      browserHistory.push('/');
        return [
          ...state,
          Object.assign({}, {token: action.credentials.token, userName: action.credentials.user.username, 
          isLoggedIn: true, email: action.credentials.user.email, firstName: action.credentials.user.firstname, lastName: action.credentials.user.lastname, statusText: null})
        ];
    case types.LOGOUT:
		browserHistory.push('/login');
        return [initialState];
    case types.LOGIN_FAILURE:
        return [
          ...state,
          Object.assign({}, {token: null, userName:null, isLoggedIn: false, statusText: 'Authentication error, wrong username or password'})
        ];
    default:
          return state;
  }
};