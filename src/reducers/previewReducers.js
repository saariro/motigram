
import * as types from '../constants/actionTypes';


export default (state =  null, action) => {
  switch (action.type){
    case types.PREPARE_UPLOAD:
        return action.image["0"];
	case types.PROFILE_OPEN:
		return null;
    default:
          return state;
  }
};