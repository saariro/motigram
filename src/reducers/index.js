// ./src/reducers/index.js
import { combineReducers } from 'redux';
import credentials from './loginReducers';
import posts from './postReducers';
import isOpen from './modalReducers';
import file from './previewReducers';
import files from './fileReducers';
import index from './increaseReducers';

export default combineReducers({
  credentials: credentials,
  posts: posts,
  isOpen: isOpen,
  file: file,
  files: files,
  index: index
});
