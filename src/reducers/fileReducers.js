
import * as types from '../constants/actionTypes';

const initialState = [{
    file: null,
    uploadedFile: null
}];


export default (state =  initialState, action) => {
  switch (action.type){
    case types.GET_FILE_SUCCESS:
        return {file: action.file, uploadedFile: null};
    case types.GET_FILE_FAILURE:
        return state;    
    case types.POST_FILE_SUCCESS:
        return {file: state.file, uploadedFile: action.posts._id};
    case types.POST_FILE_FAILURE:
        return state;
    case types.LOGOUT:
        return initialState;
    default:
          return state;
  }
};