
import * as types from '../constants/actionTypes';

const initialState = [{
    photo: null,
    caption: null,
    likes: 0,
    creator: null
}];


export default (state =  initialState, action) => {
  switch (action.type){
    case types.GET_POST_SUCCESS:
        return state= action.posts;
    case types.GET_POST_FAILURE:
        return state;    
	case types.UPDATE_POST_SUCCESS:
        return [
          ...state,
          Object.assign({}, action.posts)
        ];
    case types.UPDATE_POST_FAILURE:
        return state;
    case types.LOGOUT:
        return [initialState];
    default:
          return state;
  }
};