
##Motigram

A test project created by Rolle Saarinen for Motius. This is built on top of boilerplate react project created by [Cory House](https://github.com/coryhouse/react-slingshot)

Most of the original scaffolding and helper webpack commands are still in place, though most of them were not used in this project. A documentation on how to use those can be found from 'docs' folder.

I didn't have time to straighten out all the bugs from this version, and one notable bug that I couldn't fix was that the like button doesn't still work. Though, I couldn't get update post endpoint
to work with Postman either, so the likely reason is that the URL or body is simply missing something there. Also, this app was mostly developed on Chrome, and seems to work best on that, furthermore, I couldn't get it
to work at all on Internet Explorer 11, so I'd recommend to avoid using that.

## Installation
1. **Install the dependencies**. `npm install`
2. **Run the app**. `npm start -s`